#pragma once

#include <iostream>
#include <vector>

// Рекурсивные сортировки.

/**
 * Слияние двух подмассивов в один.
 * @param arr Сортируемый массив.
 * @param left Левая граница первого подмассива, участвующего в слиянии.
 * @param m Правая граница первого подмассива, участвующего в слиянии.
 * @param right Правая граница второго подмассива, участвующего в слиянии.
 */
void merge(std::vector<int> &arr, int left, int m, int right) {
    int i = left, j = m + 1;

    std::vector<int> answer;
    answer.reserve(right - left + 1);
    while (i <= m || j <= right) {
        // Выбираем наименьший.
        if (i <= m && j <= right) {
            if (arr[i] < arr[j]) {
                answer.push_back(arr[i++]);
            } else {
                answer.push_back(arr[j++]);
            }
        } else if (i <= m) {
            answer.push_back(arr[i++]);
        } else {
            answer.push_back(arr[j++]);
        }
    }

    // Вносим части с правильным порядком.
    for (int k = left; k <= m; k++) {
        arr[k] = answer[k - left];
    }
    for (int k = m + 1; k <= right; k++) {
        arr[k] = answer[k - left];
    }
}

/**
 * Сортировка слиянием.
 * @param arr Сортируемый массив.
 * @param left Левая граница сортируемого массива.
 * @param right Правая граница сортируемого массива.
 */
void mergeSort(std::vector<int> &arr, int left, int right) {
    int m = left + (right - left) / 2;
    if (left < m) {
        mergeSort(arr, left, m);
    }
    if (m + 1 < right) {
        mergeSort(arr, m + 1, right);
    }

    merge(arr, left, m, right);
}

/**
 * Быстрая сортировка с разбиением Хоара.
 * @param arr Сортируемый массив.
 * @param left Левая граница сортируемого массива.
 * @param right Правая граница сортируемого массива.
 */
void quickSort(std::vector<int> &arr, int left, int right) {
    int i = left, j = right, k = left + (right - left) / 2;
    int x = arr[k];

    while (i <= j) {
        while (arr[i] < x) {
            i++;
        }
        while (arr[j] > x) {
            j--;
        }

        if (i <= j) {
            std::swap(arr[i], arr[j]);
            i++;
            j--;
        }
    }

    if (left < j) {
        quickSort(arr, left, j);
    }

    if (right > i) {
        quickSort(arr, i, right);
    }
}

/**
 * Обеспечивает основное свойство кучи для i-ой вершины массива.
 * @param arr Массив.
 * @param i Номер вершины.
 * @param heap_size Размер кучи.
 */
void Heapify(std::vector<int> &arr, int i, int heap_size) {
    int left = 2 * i + 1, right = 2 * i + 2, largest = i;

    // Ищем максимальный элемент из родителя и двух детей.
    if (left < heap_size && arr[left] > arr[i]) {
        largest = left;
    }
    if (right < heap_size && arr[right] > arr[largest]) {
        largest = right;
    }

    // Если максимальный - кто-то из детей, то обмениваем его с родителем и повторяем всё для ребенка.
    if (largest != i) {
        std::swap(arr[i], arr[largest]);
        if (largest < (heap_size / 2)) { // если вершнина - не лист (есть дети, которых будем проверять)
            Heapify(arr, largest, heap_size);
        }
    }
}

/**
 * Строит кучу из исходного не сортированного массива.
 * @param arr Массив.
 */
void BuildHeap(std::vector<int> &arr) {
    for (int i = (arr.size() / 2) - 1; i >= 0; i--) {
        Heapify(arr, i, arr.size());
    }
}

/**
 * Пирамидальная сортировка.
 * @param numbers Сортируемый массив.
 * @param left Левая граница сортируемого массива.
 * @param right Правая граница сортируемого массива.
 */
void heapSort(std::vector<int> &numbers, int left, int right) {
    int heap_size = numbers.size();
    BuildHeap(numbers);
    for (int i = right; i >= left; i--) {
        std::swap(numbers[0], numbers[i]);
        heap_size--;
        Heapify(numbers, 0, heap_size);
    }
}