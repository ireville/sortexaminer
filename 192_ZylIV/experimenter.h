#pragma once

#include <iostream>
#include <vector>
#include <functional>
#include <chrono>
#include <set>
#include <map>
#include <fstream>
#include "iterableSorts.h"
#include "linearSorts.h"
#include "recursiveSorts.h"
#include "arrayMethods.h"

/**
 * Класс, умеющий проводить эксперимент и содеражащий всё необходимое для этого.
 */
class Experimenter {
public:
    Experimenter() {
        srand(time(NULL));
        initializeFunctions();
    }


    /**
     * Запускает эксперименты для каждого вида массива и для каждого значения длины массива.
     */
    void conductExperiment() {
        // Проходимся по каждому виду массива.
        for (int flag = 0; flag < ARRAY_TYPES_CNT; flag++) {
            // Проходимся по каждому нужному значению длины.
            for (int len = 50; len <= 300; len += 50) {
                doExperiment(flag, len);
            }

            for (int len = 1100; len <= 4100; len += 1000) {
                doExperiment(flag, len);
            }
        }
    }

    /**
     * Записывает результаты в .csv файл.
     */
    void writeToFile() {
        std::fstream fout;
        fout.open("192_ZylIV_results_table.csv", std::ios::out);

        // Формируем заголовочную строку.
        fout << "Array length";
        for (const auto &pair : results) {
            // Записываем тип сортировки и вид массива.
            fout << ";" + _functions[pair.first.first].first + " | " + _flag_names[pair.first.second];
        }
        fout << std::endl;

        // Вписываем все замеры.
        int i = 0;
        for (int len : length_values) {
            fout << std::to_string(len);
            for (auto pair : results) {
                fout << ";" + std::to_string(pair.second[i]);
            }
            i++;
            fout << std::endl;
        }

        fout.close();
    }

private:
    static constexpr int ARRAY_TYPES_CNT = 4; // Количество видов массивов.
    static constexpr int SORT_TYPES_CNT = 8; // Количество типов сортировки.
    static constexpr int EXPERIMENTS_CNT = 70; // Количество проводимых идентичных атомарных экспериментов.
    std::set<int> length_values; // Все значения длин массивов, которые были в экспериментах.

    // Все функции-сортировки.
    std::vector<std::pair<std::string, std::function<void(std::vector<int> &, int, int)>>> _functions;

    // Словарь для видов массивов.
    std::vector<std::string> _flag_names = {"Random 0-5", "Random 0-4000", "Almost sorted", "Reversed"};

    // Таблица результатов (словарь из пар (тип сортировки, вид массива) -- время в наносекундах).
    std::map<std::pair<int, int>, std::vector<long double>> results;

    /**
     * Инициализирует массив из функций-сортировок.
     */
    void initializeFunctions() {
        _functions.emplace_back("Bubble Sort", bubbleSort);
        _functions.emplace_back("Select Sort", selectSort);
        _functions.emplace_back("BinaryInsert Sort", binaryInsertSort);
        _functions.emplace_back("Counting Sort", countingSort);
        _functions.emplace_back("Radix Sort", radixSort);
        _functions.emplace_back("Merge Sort", mergeSort);
        _functions.emplace_back("Quick Sort", quickSort);
        _functions.emplace_back("Heap Sort", heapSort);
    }

    /**
     * Проверяет, совпадают ли два массива.
     * @param arr Массив, отсортированный кастомной сортировкой.
     * @param sorted Массив, отсортированный корректно.
     * @return Истину, если массивы совпадают (то есть кастомная сортировка прошла корректно), ложь - иначе.
     */
    bool checkWhetherSorted(const std::vector<int> &arr, const std::vector<int> &sorted) {
        for (int i = 0; i < arr.size(); i++) {
            if (arr[i] != sorted[i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Замеряет время на выполнение атомарного эксперимента и проверяет, правильно ли прошла сортировка.
     * @param my_sort Сортировка, которая будет запущена (если точнее - пара из названия и функции сортировки).
     * @param arr Сортируемый массив.
     * @param sorted_arr Уже верно отсортированный массив (для проверки корректности кастомной сортировки).
     * @return Время в наносекундах, затраченное на эксперимент.
     */
    long long experiment(const std::pair<std::string, std::function<void(std::vector<int> &, int, int)>> &my_sort,
                         std::vector<int> arr, const std::vector<int> &sorted_arr) {
        // Запускаем счётчик времени.
        auto start = std::chrono::high_resolution_clock::now();

        // Проводим эксперимент.
        my_sort.second(arr, 0, arr.size() - 1);

        // Останавливаем время.
        auto stop = std::chrono::high_resolution_clock::now();

        // Проверяем корректность выполненной сортировки.
        if (!checkWhetherSorted(arr, sorted_arr)) {
            throw std::runtime_error("Array sorted incorrectly! " + my_sort.first);
        }

        // Возвращаем время, затраченное сортировкой при проведении эксперимента.
        return std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count();
    }

    /**
     * Запускает эксперименты для каждого типа сортировки.
     * @param flag Тип массива для эксперимента.
     * @param len Длина массива для эксперимента.
     */
    void doExperiment(int flag, int len) {
        // Нужно для дальнейшего сохранения данных.
        length_values.insert(len);

        // Генерируем массив.
        std::vector<int> arr = generateArray(flag, len);

        // Проходимся по каждому типу сортировки.
        for (int sortType = 0; sortType < SORT_TYPES_CNT; sortType++) {
            // Формируем правильно отсортированный массив.
            std::vector<int> sorted_array(arr.begin(), arr.end());
            std::sort(sorted_array.begin(), sorted_array.end());

            /* Запускаем серию экспериментов на заданном массиве определенного типа и длины и для заданного выше типа
               сортировки. */
            long long sum = 0;
            // Проводим EXPERIMENTS_CNT аналогичных экспериментов.
            for (int exp = 0; exp < EXPERIMENTS_CNT + 3; exp++) {
                // Проводим эксперимент.
                long long exp_result =
                        experiment(_functions[sortType], arr, sorted_array);

                // Запускаем 3 раза в холостую (не учитываем результаты), чтобы получить оптимизацию компилятора.
                if (exp > 3) {
                    sum += exp_result;
                }
            }

            // Считаем среднее значение времени по всем экспериментам.
            long double avg_result = ((long double) sum) / ((long double) EXPERIMENTS_CNT);

            // Заносим результаты.
            results[std::make_pair(sortType, flag)].push_back(avg_result);
        }
    }
};
