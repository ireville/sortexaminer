#pragma once

#include <iostream>
#include <vector>

// Итерационные сортировки.

/**
 * Сортировка пузырьком.
 * @param arr Сортируемый массив.
 * @param left Левая граница сортируемого массива.
 * @param right Правая граница сортируемого массива.
 */
void bubbleSort(std::vector<int> &arr, int left, int right) {
    for (int i = left; i <= right; i++) {
        for (int j = left; j < right - i; j++) {
            if (arr[j] > arr[j + 1]) {
                std::swap(arr[j], arr[j + 1]);
            }
        }
    }
}

/**
 * Сортировка вставками.
 * @param arr Сортируемый массив.
 * @param left Левая граница сортируемого массива.
 * @param right Правая граница сортируемого массива.
 */
void selectSort(std::vector<int> &arr, int left, int right) {
    for (int i = left; i <= right; i++) {
        int min_ind = i;
        for (int j = i + 1; j <= right; j++) {
            // Находим наименьший элемент в неотсортированной части массива.
            if (arr[j] < arr[min_ind]) {
                min_ind = j;
            }
        }

        // Меняем его местами с первым в неотсортированной части.
        if (arr[i] != arr[min_ind]) {
            std::swap(arr[i], arr[min_ind]);
        }
    }
}

/**
 * Бинарный поиск.
 * @param A Массив в котором производится поиск.
 * @param key Значение ключа, который ищем.
 * @param right Правая граница массива.
 * @return Индекс найденного элемента.
 */
int BinSearch(const std::vector<int> &A, int key, int right) {
    int left = 0, mid;

    while (left < right) {
        mid = left + (right - left) / 2;

        // Если текущий элемент больше требуемого, то сдвигаем правую границу.
        if (A[mid] > key) {
            right = mid;
        } else { // Если текущий элемент меньше требуемого, то сдвигаем левую границу.
            left = mid + 1;
        }
    }

    return left;
}

/**
 * Сортировка бинарными вставками.
 * @param arr Сортируемый массив.
 * @param left Левая граница сортируемого массива.
 * @param right Правая граница сортируемого массива.
 */
void binaryInsertSort(std::vector<int> &arr, int left, int right) {
    for (int i = 0; i <= right; i++) {
        // Вставляемый элемент.
        int key = arr[i];
        int position = BinSearch(arr, key, i);

        for (int j = i; j > position; j--) {
            arr[j] = arr[j - 1];
        }

        arr[position] = key;
    }
}