#pragma once

#include <iostream>
#include <vector>

// Линейные сортировки.

/**
 * Сортировка подсчетом.
 * @param numbers Сортируемый массив.
 * @param left Левая граница сортируемого массива.
 * @param right Правая граница сортируемого массива.
 */
void countingSort(std::vector<int> &numbers, int left, int right) {
    // Находим минимум и максимум.
    int max = -1, min = 1000001;
    for (int i = 0; i <= right; i++) {
        if (numbers[i] < min) {
            min = numbers[i];
        }
        if (numbers[i] > max) {
            max = numbers[i];
        }
    }

    // Задаём размер вспомагательного массива.
    int k = max - min + 1;

    // Создаём и заполняем вспомогательный массив.
    std::vector<int> c(k);
    for (int i = 0; i <= right; i++) {
        c[numbers[i] - min] += 1;
    }

    for (int i = 1; i < k; i++) {
        c[i] += c[i - 1];
    }

    // Создаём итоговый массив, в котором будет отсортированный исходный.
    std::vector<int> b(right + 1);
    for (int i = right; i >= 0; i--) {
        b[c[numbers[i] - min] - 1] = numbers[i];
        c[numbers[i] - min] -= 1;
    }

    // Записываем ответ в изначальный массив.
    for (int i = 0; i <= right; i++) {
        numbers[i] = b[i];
    }
}

/**
 * Цифровая сортировка.
 * @param numbers Сортируемый массив.
 * @param left Левая граница сортируемого массива.
 * @param right Правая граница сортируемого массива.
 */
void radixSort(std::vector<int> &numbers, int left, int right) {
    // Заводим тип данных, описывающий наше число.
    union Un {
        unsigned int num;
        unsigned char arr[4];
    };

    // Создаём массив этого типа данных с числами из нашего массива
    std::vector<Un> uns(numbers.size());
    for (int i = 0; i < numbers.size(); i++) {
        uns[i].num = numbers[i];
    }

    // Проходясь по значениям каждого из разрядов, сортируем подсчётом наши числа.
    for (int j = 0; j < 4; j++) {
        // Находим минимум и максимум по нужному разряду.
        int max = -1, min = 256;
        for (int i = 0; i < numbers.size(); i++) {
            if (uns[i].arr[j] < min) {
                min = uns[i].arr[j];
            }
            if (uns[i].arr[j] > max) {
                max = uns[i].arr[j];
            }
        }

        // Задаём размер вспомогательного массива.
        int k = max - min + 1;

        // Создаём и заполняем вспомогательный массив.
        std::vector<int> c(k);
        for (int i = 0; i < numbers.size(); i++) {
            c[uns[i].arr[j] - min] += 1;
        }

        for (int i = 1; i < k; i++) {
            c[i] += c[i - 1];
        }

        // В исходный массив записываем остортированный по разряду исходный массив.
        for (int i = numbers.size() - 1; i >= 0; i--) {
            numbers[c[uns[i].arr[j] - min] - 1] = uns[i].num;
            c[uns[i].arr[j] - min] -= 1;
        }

        // Записываем ответ в массив для дальнейшей сортировки.
        for (int i = 0; i < numbers.size(); i++) {
            uns[i].num = numbers[i];
        }
    }
}