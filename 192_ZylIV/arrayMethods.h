#pragma once

#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>

/**
 * Создаёт массив заданной длины со случаными числами в заданном диапазоне.
 * @param len Длина требуемого массива.
 * @param min Минимальное возможное значение элемента массива.
 * @param max Максимальное возможное значение элемента массива.
 * @return Сгенерированный массив.
 */
std::vector<int> createArray(int len, int min, int max){
    std::vector<int> arr(len);

    for(int i = 0; i < len; i++){
        arr[i] = std::rand() % (max - min + 1) + min;
    }

    return arr;
}

/**
 * Создаёт массив с элементами, упорядоченными по невозрастанию.
 * @param len Длина требуемого массива.
 * @param min Минимальное возможное значение элемента массива.
 * @param max Максимальное возможное значение элемента массива.
 * @return Сгенерированный массив.
 */
std::vector<int> createReversedArray(int len){
    std::vector<int> arr;
    arr.reserve(len);

    for(int i = len; i >= 1; i--){
        arr.push_back(i);
    }

    return arr;
}

/**
 * Создаёт почти остортированный массив, где в каждой тысяче элементов меняются местами 7 (например) пар элементов.
 * @param len Длина требуемого массива.
 * @param min Минимальное возможное значение элемента массива.
 * @param max Максимальное возможное значение элемента массива.
 * @return Сгенерированный массив.
 */
std::vector<int> createAlmostSortedArray(int len, int min, int max){
    // Создаём и полностью сортируем массив.
    std::vector<int> arr = createArray(len, min, max);
    std::sort(arr.begin(), arr.end());

    // Устраиваем обмен между N парами рандомных элементов.
    int N = 7;
    for(int i = 0; i < len; i+= 1000){
        for(int j = 0; j < N; j++){
            std::swap(arr[(rand() % 1000) % (len - i) + i], arr[(rand() % 1000) % (len - i) + i]);
        }
    }

    return arr;
}

/**
 * Создаёт требуемый вид массива в зависимости от флага.
 * @param flag Обозначает вид требуемого массива.
 * @param len Длина требуемого массива.
 * @return Сгенерированный массив.
 */
std::vector<int> generateArray(int flag, int len){
    switch(flag){
        case 0:
            return createArray(len, 0, 5);
        case 1:
            return createArray(len, 0, 4000);
        case 2:
            return createAlmostSortedArray(len, 0, 4100);
        case 3:
            return createReversedArray(len);
        default:
            return std::vector<int>();
    }
}